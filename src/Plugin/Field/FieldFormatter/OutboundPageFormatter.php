<?php

namespace Drupal\outbound\Plugin\Field\FieldFormatter;

use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'outbound_page_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "outbound_page_formatter",
 *   label = @Translation("Outbound page"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class OutboundPageFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // The entity this link is attached to.
    $entity = $items->getEntity();

    // Current path entity information.
    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();

    // Check which entity type is used and update entity_link to use the
    // correct route url.
    switch ($entity_type) {
      case 'node':
        $entity_link = Url::fromRoute(
          'entity.node.canonical',
          ['node' => $entity_id],
        );
        break;

      case 'taxonomy_term':
        $entity_link = Url::fromRoute(
          'entity.taxonomy_term.canonical',
          ['taxonomy_term' => $entity_id],
        );
        break;
    }

    // Change url to string which is passed into template file.
    $previous_page_url = $entity_link->toString();

    // TODO: Make configurable.
    $previous_page_link_title = $this->t(
      'Back to @title',
      [
        '@title' => $entity->label(),
      ]
    );

    foreach ($items as $delta => $item) {
      // By default use the full URL as the link text.
      $outbound_url = $this->buildUrl($item);
      $outbound_link_title = $outbound_url->toString();

      // If the link text field value is available, use it for the text.
      if (!empty($item->title)) {
        // Unsanitized token replacement here because the entire link title
        // gets auto-escaped during link generation in
        // \Drupal\Core\Utility\LinkGenerator::generate().
        $outbound_link_title = \Drupal::token()->replace($item->title, [$entity->getEntityTypeId() => $entity], ['clear' => TRUE]);
      }

      $elements[$delta] = [
        '#theme' => 'outbound_page_formatter',
        '#previous_page_url' => $previous_page_url,
        '#previous_page_link_title' => $previous_page_link_title,
        '#outbound_url' => $outbound_url,
        '#outbound_link_title' => $outbound_link_title,
      ];

      if (!empty($item->_attributes)) {
        // Set our RDFa attributes on the <a> element that is being built.
        $url->setOption('attributes', $item->_attributes);

        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }
    }

    return $elements;
  }

}
