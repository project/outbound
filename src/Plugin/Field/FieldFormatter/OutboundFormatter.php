<?php

namespace Drupal\outbound\Plugin\Field\FieldFormatter;

use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'outbound_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "outbound_formatter",
 *   label = @Translation("Outbound"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class OutboundFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $entity = $items->getEntity();

    // Current path entity information.
    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();
    $entity_link = '';

    // Check which entity type is used and update entity_link to use the
    // correct route url based on our custom routes.
    switch ($entity_type) {
      case 'node':
        $entity_link = Url::fromRoute(
          'outbound.outbound_page_node',
          ['node' => $entity_id],
        );
        break;

      case 'taxonomy_term':
        $entity_link = Url::fromRoute(
          'outbound.outbound_page_taxonomy_term',
          ['taxonomy_term' => $entity_id],
        );
        break;
    }

    // Change url to string which is passed into template file.
    $url_outbound = $entity_link->toString();

    foreach ($items as $delta => $item) {
      // By default use the full URL as the link text.
      $link_url = $this->buildUrl($item);
      $link_title = $link_url->toString();

      // If the link text field value is available, use it for the text.
      if (!empty($item->title)) {
        // Unsanitized token replacement here because the entire link title
        // gets auto-escaped during link generation in
        // \Drupal\Core\Utility\LinkGenerator::generate().
        $link_title = \Drupal::token()->replace($item->title, [$entity->getEntityTypeId() => $entity], ['clear' => TRUE]);
      }

      $elements[$delta] = [
        '#theme' => 'outbound_formatter',
        '#url' => $link_url,
        '#link_title' => $link_title,
        '#url_outbound' => $url_outbound,
      ];
    }

    return $elements;
  }

}
