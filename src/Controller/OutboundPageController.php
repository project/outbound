<?php

namespace Drupal\outbound\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A controller for the outbound page.
 */
class OutboundPageController extends ControllerBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * Returns a page title.
   */
  public function getTitle() {
    $config = $this->configFactory->get('system.site');
    $sitename = $config->get('name');

    $title = $this->t('You are now leaving @sitename', [
      '@sitename' => $sitename,
    ]);
    return $title;
  }

}
