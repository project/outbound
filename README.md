# @coldfrontlabs/outbound

Want to inform a user that they are leaving your site after clicking a link? Use outbound to create links that will transfer users to an outbound page before they visit the link itself.

The module provides two outbound link field formatters and a view mode. Currently this module only works on nodes. Will require additional work for other entities.

Link Formatters:
- Outbound (for the page you are linking from)
- Outbound Page (For the page between the original page and the link)

Both formatters include twig templates in the module that can be overridden. The Outbound formatter will change your link to point towards /node/{nodeid}/outbound. So each node can contain it's own outbound page.

View Mode
- Outbound (Build out the outbound page with field display, layout builder, or other available layout tools);

## How to set this up?

1. Create a link field that will be used for these outbound links.
2. Enable the link field on the display which will be showing the original page. Apply the "Outbound" formatter to that link.
3. Enable the Outbound view mode on your content type.
4. On the Outbound view mode, at minimum, show your link field and apply the "Outbound Page" formatter. This will contain a "back to content" link and the original link you've stored in the link field.

After saving both these changes, all the nodes in your content type will have a /outbound page when the link field is populated.
